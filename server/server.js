"use strict";

import express from 'express';
import path from 'path';
import mysql from 'mysql';
import bodyParser from 'body-parser';
const app = express();
const PORT = 8081;

app.listen(PORT, () => {
  console.log('Running...');
})

app.use(express.static(path.resolve() + '/server'));
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());


//app.use(express.json());

var db = mysql.createConnection({
  host: "db",
  user: "admin",
  password: "password",
  database: 'prog2053-proj'
});

db.connect(function (err) {
  if (err) {
    throw err;
  }
  console.log("Connected!");
});

app.get('/', (req, res) => {
  res.send("Hello world");
})

// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.get('/getUsers', function (req, res) {
  let sql = `SELECT * FROM users`;
  let query = db.query(sql, (err, results) => {
    if(err) throw err;
    console.log(results);
    res.send(results);
  });
});

app.get('/post/:pid', (req, res) => {
  let sql = `SELECT * FROM posts WHERE pid = ${req.params.pid}`;
  let query = db.query(sql, (err, results) => {
    if(err) throw err;
    console.log(results);
    res.send(results);
  });
});

app.get('/getPosts',(req, res) => {
  let sql = `SELECT * FROM posts`;
  let query = db.query(sql, (err, results) => {
    if(err) throw err;
    console.log(results);
    res.send(results);
  });
});

app.get('/comments/:pid', (req, res) => {
  let sql = `SELECT * FROM comments WHERE post = ${req.params.pid}`;
  let query = db.query(sql, (err, results) => {
    if(err) throw err;
    console.log(results);
    res.send(results);
  });
});

app.get('/commentsUser/:uid', (req, res) => {
  let sql = `SELECT * FROM comments WHERE user = ${req.params.uid}`;
  let query = db.query(sql, (err, results) => {
    if(err) throw err;
    console.log(results);
    res.send(JSON.stringify(results));
  });
});

app.post('/createPost', (req, res) => {
  let sql = `INSERT INTO posts (content, title, user) VALUES ("${req.body.text}","${req.body.title}",${req.body.user})`
  let query = db.query(sql, (err, results) => {
    if(err) throw err;
    //console.log(results);
    console.log(results.insertId)
    res.send(JSON.stringify(results.insertId));
  })
})


app.post('/createComment', (req, res) => {
  if(req.body.pid != 0){
    let sql = `INSERT INTO comments (comment, post, user) VALUES ("${req.body.content}",${req.body.pid},${req.body.user})`
    let query = db.query(sql, (err, results) => {
      if(err) throw err;
      console.log(results);
      res.send('Comment added');
    })
  } else {
    res.send('Error, invalid post id');
    console.log('Error, invalid post id');
  }
})