/*
* npm install --save @vaadin/router
*/

import {Router} from '@vaadin/router';
import './src/frontPage.js';
import './src/postPage.js';
import './src/template.js';
import './src/not-found.js';
import './src/new-post-page.js';
import './src/postComponent';
import './src/PostList.js';
import './src/UserPage.js';
import './src/adminPage.js';
import './src/login.js';

const outlet = document.getElementById('outlet');
const router = new Router(outlet);

router.setRoutes([
  {path: '/', component: 'template-tp',
    children: [
      {path: '/', component: 'front-page'},
      {path: '/newPost', component: 'new-post-page'},
      {path: '/post-:pid', component: 'post-page'},
      {path: '/List', component: 'post-list'},
      {path: '/userpage', component: 'user-page'},
      {path: '/admin', component: 'admin-page'},
      {path: '/login', component: 'tamp-late'},
      {path: '(.*)', component: 'not-found'}
    ]}
]);