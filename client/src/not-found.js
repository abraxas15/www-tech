import { LitElement, html, css } from 'lit-element';

class NotFound  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <h1>Error, page not found</h1>
    `;
  }
}

customElements.define('not-found', NotFound);