import { LitElement, html, css } from 'lit-element';

class PostCOmponent  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
      title: String,
      user: String,
      content: String,
      pid: String
    };
  }

  constructor() {
    super();
    this.title = "unknown";
    this.user = "unkown";
    this.content = "unknown";
    this.pid = 0;
  }

  render() {
    return html`
      <div class="card mb-xl-3" style="width: 100%;">
        <div class="card-body">
          <h5 class="card-title">${this.title}</h5>
          <p id="postUser">User #${this.user}</p>
          <p class="card-text">${this.content}</p>
          <div class="input-group mb-3">
            <input type="text" id="text-field" class="form-control" placeholder="Comment">
            <div class="input-group-append">
              <button class="btn btn-outline-secondary" type="button" id="button-addon2" @click="${this.register}">Reply</button>
            </div>
          </div>
        </div>
      </div>
    `;
  }

  register(e) {
    const user = 1;
    const pid = this.pid;
    const content = this.shadowRoot.getElementById("text-field").value;

    const data = {user, content, pid};

    fetch(`${window.MyAppGlobals.serverURL}createComment`, {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then((res) => {
      console.log("this is res", res)
    }).catch((err) => {
      console.log(err)
    })
    location.reload();
  }
}

customElements.define('post-component', PostCOmponent);