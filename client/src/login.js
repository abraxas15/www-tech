import { LitElement, html, css } from 'lit-element';
import './template.js';

class Templat  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
      uid: Number,
      email: String,
      user: String,
      password: String
    };
  }

  constructor() {
    super();
    this.email = "unknown";
    this.user = "unkown";
    this.password = "unknown";
    this.uid = 0;
  }

  render() {
    return html`
    <div class="loginDiv">
      <input class="userName" type="text" placeholder="email" @change=${e => this.email = e.target.value}></input><br>
      <input class="password" type="text" placeholder="password" @change=${r =>  this.password  = r.target.value}></input><br>
      <button @change="${this.register()}">Sjekk bruker</button>
    </div>
    <p id="demo"></p>
    `;
  }


  register() {
    console.log("Kjorer...")
    fetch(`${window.MyAppGlobals.serverURL}getUsers`
      ).then( res => res.json()
      ).then(data => {
        for(let usernr in data){
          let tmpUser = document.createElement("logregister-component");
          tmpUser.password = data[usernr].password;
          tmpUser.email =data[usernr].email;
          tmpUser.uid = data[usernr].uid;
          console.log(tmpUser.email);
          console.log(tmpUser.password);
          if (this.email == tmpUser.email && this.password == tmpUser.password){
            location.replace("/");
          }
          else{ 
            console.log("Feil");
          }
        }
      });

  }
}
customElements.define('tamp-late', Templat);