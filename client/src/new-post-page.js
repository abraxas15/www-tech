import { LitElement, html, css } from 'lit-element';

class NewPostPage  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <div class="form-group">
        <label for="postTitle">Post Title</label>
        <input type="text" class="form-control" id="titleBox" required>
      </div>
      <div class="form-group" style="width: 100%;">
          <label for="postContent">Post Content</label>
          <textarea class="form-control" style="width: 100%; height: 500px;" id="textBox" required></textarea>
      </div>
      <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1" required>I'm not a robot</label>
      </div>
      <button type="button" class="btn btn-primary" @click="${this.register}">Post</button>
    `;
  }

  register(e) {
    const text = this.shadowRoot.getElementById("textBox").value;
    const title = this.shadowRoot.getElementById("titleBox").value;
    const user = 1;

    const data = {text, title, user};

    fetch(`${window.MyAppGlobals.serverURL}createPost`, {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => res.json()
    ).then(data => {
      let redirect = "/post-" + data;
      location.replace(redirect);
    })

  }
}


customElements.define('new-post-page', NewPostPage);