import { LitElement, html, css } from 'lit-element';

export class rulesInfo extends LitElement {

    static get properties() {
        return {
            route: { type: String },
    		params: { type: Object },
			query: { type: Object },
			data: { type: Object }
        };
	}

	static get routes() {
		return [
			{
			name: "home",
			pattern: "",
			data: { title: "Home" }
			},
			{
			name: "post",
			pattern: "post/:id"
			},
			{
			name: "not-found",
			pattern: "*"
			}
		];
	  }

    constructor() {
        super();
		this.route = "";
		this.params = {};
		this.query = {};
		this.data = {};
	}
	
	router(route, params, query, data) {
		this.route = route;
		this.params = params;
		this.query = query;
		this.data = data;
		console.log(route, params, query, data);
	}

    static get styles() {
        return [
            css`
                #DIN CSS HER
            `,
        ]
    }


    render() {
        return html`
            <!-- DIN HTML HER -->
            <!-- Action: i HTML-formen er ikke nødvendig så lenge du bruker: -->
			<!-- Gi button din property: @click="${this.register}" -->
			<ul>
                <li>
                    <p><strong>Don't be rude</strong> but being nude is fine, as long as you're at home</p>
            	</li>
                <li>
                    <p><strong>Do not walk around naked in the public</strong> people generally dont appreciate that</p>
                </li>
                <li>
                    <p><strong>Read the rules of a community before poting</strong> these rules right here</p>
                </li>
                <li>
                    <p><strong>Don't break the law</strong> The police wil get sad</p>
                </li>
                <li>
                    <p><strong>Fianlly, don't make people sad</strong></p>
                </li>
			</ul>

			<app-link href="./post/2">post/2</app-link>

			<app-main active-route=${this.route}>
				<h1 route="post">post ${this.params.id}</h1>
			</app-main>
            `;
    }


    register(e) {
      console.log(
        fetch(`${window.MyAppGlobals.serverURL}post/1`
        ).then( res => res.json()
        ).then(data => {
          console.log(data);
        }));
    }
}
customElements.define('rules-info', rulesInfo);