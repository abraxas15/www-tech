import { LitElement, html, css } from 'lit-element';
import './PostListComponentUser.js';
import './commentComponentUser.js';

class UserPage  extends LitElement {

    static get styles() {
        return css`
          .fakeimg {
            height: 200px;
            background: #aaa;
          }
          .navbar-custom {
            background-color: #5D5C61;
          }
          .jumbotron-custom {
            background-color: #B1A296;
            margin-bottom: 0;
          }
            .jumbotron-custom-header{
            background-color: #379683;
          }
          
          
          /* Box styles */
          .myBox {
           border: none;
           padding: 5px;
           font: 24px/36px sans-serif;
           width: 800px;
           height: 300px;
         }
         .myNav {
           line-height: 30px;
           background-color: #eeeeee;
           width: 15%;
           height: 200px;
           float: left;
           padding: 5px;
         }
     
         .myButton {
           width: 99%;
           height: 33%;
           text-align: center;
           font-size: 1em;
         }
         .mySection{
           width: 70%;
           height: 320px;
           padding: 5px;
           overflow: scroll;
         }`;


      }
    

      get properties() {
        return {
          antallPost: Number,
          antallComment: Number,
        };
      }

      static get properties() {
        return {
          userNr: Number,
        };
      }
    
      constructor() {
        super();
        this.userNr = 1;  //is hard coded for now
        this.antallPost = 0;
        this.antallComment = 0;
      }

      render() {
      return html`
      <div class="container" style="margin-top:30px">
      <div class="row">
        <nav class="myNav"> 
          <button class="myButton" onclick="getUserStats()">User stats</button>
          <button class="myButton" onclick="getUserLikes()">My likes/dislikes</button>
          <button class="myButton" onclick="getUserPostComments()">My comments/posts</button>
         </nav>
        
         
         
         <section class="mySection">
         ${this.register(this.location.params.uid)}
           <p id="P1">Your user stats: <br> you have x post, and x comments.</p>
           <div id="B1" class="myBox">
            <p>Your post: <div class="postDiv"></div>
            <br><br>
            Your comments: <div class="commentDiv"></div></p>
           </div>
         </section> 
          </div>
       </div>    
       `;
      }

      register() {
        console.log("Kjorer...")
      fetch(`${window.MyAppGlobals.serverURL}getPosts`
        ).then( res => res.json()
        ).then(data => {
          let postDiv = this.getElementsByClassName("postDiv")[0];
          for(let postnr in data){
            if(data[postnr].user == this.userNr){
            let tmpPost = document.createElement("postlist-component-user");
            tmpPost.title = data[postnr].title;
            tmpPost.pid = data[postnr].pid;
            postDiv.appendChild(tmpPost);}
          }
        });


        fetch(`${window.MyAppGlobals.serverURL}commentsUser/${this.userNr}`
        ).then( res => res.json()
        ).then(data => {
          let commentDiv = this.getElementsByClassName("commentDiv")[0];
          for(let comment in data){
           let tmpComment = document.createElement("comment-component-user");
           tmpComment.comment = data[comment].comment;
           tmpComment.post = data[comment].post;
           commentDiv.appendChild(tmpComment);
      }
    })
      }
      
    }
    
customElements.define('user-page', UserPage);



