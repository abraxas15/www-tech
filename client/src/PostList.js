import { Router } from '@vaadin/router';
import { LitElement, html, css } from 'lit-element';
import './EachPost.js';

class PostList  extends LitElement {

  static get styles() {
    return css`
    * {
      box-sizing: border-box;
    }
    
    #myInput {
      background-image: url('/css/searchicon.png');
      background-position: 10px 12px;
      background-repeat: no-repeat;
      width: 100%;
      font-size: 16px;
      padding: 12px 20px 12px 40px;
      border: 1px solid #ddd;
      margin-bottom: 12px;
    }
    
    #myUL {
      list-style-type: none;
      padding: 0;
      margin: 0;
    }
    
    #myUL li a {
      border: 1px solid #ddd;
      margin-top: -1px; /* Prevent double borders */
      background-color: #f6f6f6;
      padding: 12px;
      text-decoration: none;
      font-size: 18px;
      color: black;
      display: block
    }
    
    #myUL li a:hover:not(.header) {
      background-color: #eee;
    }
    `;
  }

  static get properties() {
    return {
      pid: Number,
      title: String,
      user: String,
      content: String
    };
  }

  constructor() {
    super();
    this.title = "unknown";
    this.user = "unkown";
    this.content = "unknown";
    this.pid = 0;
  }


  render() {
    return html`
      <br>
      <div class="postDiv" @click="${this.register()}"></div>
    `;
  }

  register() {
    console.log("Kjorer...")
  fetch(`${window.MyAppGlobals.serverURL}getPosts`
    ).then( res => res.json()
    ).then(data => {
      let postDiv = this.getElementsByClassName("postDiv")[0];
      for(let postnr in data){
        let tmpPost = document.createElement("postlist-component");
        tmpPost.title = data[postnr].title;
        tmpPost.user = data[postnr].user;
        tmpPost.content =data[postnr].content;
        tmpPost.pid = data[postnr].pid;
        postDiv.appendChild(tmpPost);
      }
    });
  }
  searchbar(){
    console.log("ff");
    let input = this.getElementsByClassName("search");
    console.log(input);
  }
}
customElements.define('post-list', PostList);
