import { LitElement, html, css } from 'lit-element';

class postlistCOmponent  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
      #button{
        border-radius: 25%;
      }
    `;
  }

  static get properties() {
    return {
    title: String,
    user: String,
    content: String,
    pid: String
    };
  }

  constructor() {
    super();
    this.user = "unkown";
    this.comment = "unknown";
  }

  render() {
    return html`
    <button class="card mb-xl-3" style="width: 100%;"><a href="./logget/post-${this.pid}"> 
     <div class="card-body">
     <h5 class="card-title">${this.title}</h5>
     <p id="postUser">User #${this.user}</p>
     <p class="card-text">${this.content}</p>
    </div>
    </a></button>
    `;
  }
}

customElements.define('postlist-component', postlistCOmponent);