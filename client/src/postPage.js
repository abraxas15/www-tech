import { LitElement, html, css } from 'lit-element';
import './postComponent.js';
import './commentComponent.js';

class PostPAge  extends LitElement {

  static get styles() {
    return css`
    `;
  }

  static get properties() {
    return {
      pid: Number,
      location: Object
    };
  }

  constructor() {
    super();
    this.pid = 0;
  }


  render() {
    return html`
      <post-component @click="${this.register(this.location.params.pid)}"></post-component>
      <div class="commentDiv"></div>
    `;
  }

  register(e) {
    fetch(`${window.MyAppGlobals.serverURL}post/${e}`
    ).then( res => res.json()
    ).then(data => {
      this.getElementsByTagName("post-component")[0].title = data[0].title;
      this.getElementsByTagName("post-component")[0].content = data[0].content;
      this.getElementsByTagName("post-component")[0].user = data[0].user;
      this.getElementsByTagName("post-component")[0].pid = e;
    });

    fetch(`${window.MyAppGlobals.serverURL}comments/${e}`
    ).then( res => res.json()
    ).then(data => {
      let commentDiv = this.getElementsByClassName("commentDiv")[0];
      for(let comment in data){
        let tmpComment = document.createElement("comment-component");
        tmpComment.comment = data[comment].comment;
        tmpComment.user = data[comment].user;
        commentDiv.appendChild(tmpComment);
      }
    })
  }
}

customElements.define('post-page', PostPAge);