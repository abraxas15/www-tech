import { LitElement, html, css } from 'lit-element';

class postlistComponentUser  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
    title: String,
    pid: String
    };
  }

  constructor() {
    super();
    this.pid = "unkown";
    this.comment = "unknown";
  }

  render() {
    return html`
     <div class="card-body">
     <button class="card mb-xl-3" style="width: 100%;"><a href="./post-${this.pid}">  
     <h3 class="card-title">post #${this.pid}: ${this.title}</h5>
    </div><br>
    `;
  }
}

customElements.define('postlist-component-user', postlistComponentUser);