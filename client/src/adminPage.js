import { LitElement, html, css } from 'lit-element';
import './userComponent.js';
class admin  extends LitElement {

  static get styles() {
    return css`
    `;
  }

  static get properties() {
    return {
      pid: String,
      location: Object
    };
  }

  constructor() {
    super();
    this.pid = 0;
  }

  render() {
    return html`

     <div class="userDiv" @click="${this.register()}">
     </div>
     
    
    `;
  }
  /*
  * Generetes the user-component tags
  *
  * 
  * */
  register(e) {
    fetch(`${window.MyAppGlobals.serverURL}getUsers`
    ).then( res => res.json()
    ).then(data => {
        let userDiv = this.getElementsByClassName("userDiv")[0];
        for(let user in data){
          let tmpUser = document.createElement("user-component");
          tmpUser.uid = data[user].uid
          tmpUser.email = data[user].email;
          tmpUser.password = data[user].password;
          tmpUser.userType = data[user].userType;
          userDiv.appendChild(tmpUser);
      console.log(data)
      }
    
  })
  
}

}

customElements.define('admin-page', admin);