import { LitElement, html, css } from 'lit-element';

class userComponent  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
      email: String,
      password: String,
      userType: String,
      pid: String,
      uid: String
    };
  }

  constructor() {
    super();
 
    this.pid = 0;
  }

  render() {
    return html`
    <h5>User ID: </h5> <p>${this.uid}</p>
    <h5>Email: </h5><p>${this.email}</p>
    <h5>Password: </h5><p>${this.password}</p>
    <h5>User type: </h5><p>${this.userType}</p>
    <br>

    `;
  }

  /*
  * Fetches data about the user
  *
  * */
  register(e) {
        fetch(`${window.MyAppGlobals.serverURL}getUsers`
        ).then( res => res.json()
        ).then(data => {
          console.log(data)
          }
        )
      }
}

customElements.define('user-component', userComponent);