import { LitElement, html, css } from 'lit-element';

class CommentCOmponent  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
    user: String,
    comment: String
    };
  }

  constructor() {
    super();
    this.user = "unkown";
    this.comment = "unknown";
  }

  render() {
    return html`
      <div class="card mb-xl-3" style="width: 100%;">
        <div class="card-body">
          <p id="postUser">User #${this.user}</p>
          <p class="card-text">${this.comment}</p>
        </div>
      </div>  
    `;
  }
}

customElements.define('comment-component', CommentCOmponent);