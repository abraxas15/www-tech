import { LitElement, html, css } from 'lit-element';

class CommentComponentUser  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
    post: String,
    comment: String
    };
  }

  constructor() {
    super();
    this.post = "unkown";
    this.comment = "unknown";
  }

  render() {
    return html`
      <div class="card mb-xl-3" style="width: 100%;"><a href="./post-${this.post}">
        <div class="card-body">
          <p id="postUser">Post #${this.post}</p>
          <p class="card-text">${this.comment}</p>
        </div>
      </div>  
    `;
  }
}

customElements.define('comment-component-user', CommentComponentUser);