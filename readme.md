## A short instruction for how to use the program

First and foremost will you need docker and the command ""docker-compose up -d", and run it to create the container. Then you would need to run "npm i" in both the client and server folders. And last run "npm install --save @vaadin/router" in client.


The project runs on localhost:8080

 -  You can create a new post, under create a new post
 -  You can view all of the posts, and also your posts, under post list
 -  See your stats, under userpage
 -  There is also an admin page where you can see all the users
    - Currently not protected, so everyone can see it
-   Can sign in/log in with a user (under development)